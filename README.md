<h1 align="center"> sARQ - 简单自动重发请求协议</h1>

<p align="center">
<img src="docs/image/logo.jpg">
</p>

<h4 align="center">
<a href="https://www.baidu.com">Website </a> &middot;
<a href="https://www.baidu.com">Online demo</a> &middot;
<a href="https://www.baidu.com">help</a> &middot;
<a href="https://www.baidu.com">Docs</a> &middot;
<a href="https://www.baidu.com">Forum</a>
</h4>

---

## 简介
sARQ是一个可靠通信协议，适合在无MMU的MCU上使用。通过停等ARQ协议保证通信双发数据传输。用户无需关注协议具体内容，
只需要通过统一接口读写数据即可，协议内部会自动完成数据校验、重发、去重等。

整个协议为纯算法实现，无底层依赖，只需要实现数据输出回调函数。

## 特性
### 点对点通信
基于两个端点之间通信，需要多点通信时创建多个协议对象即可。

### 纯C编写
纯C编写，可移植性强，需要C99及以上编译器。

### 停等ARQ协议
发送一个数据包后在规定时间内等待对方应答，超时重发。

### 全静态内存管理，无任何碎片
未使用动态内存，全部使用静态数组及堆栈完成数据缓存。无任何碎片产生，适合小内存MCU使用；保证长时间高可靠运行。

### 类对象管理
基于对象方式管理，每个通信双方皆为协议对象；支持创建任意数量对象。


## 要求
- RAM > 1K
- ROM > 3K
- Stack > 5*MTU
- Compiler >= C99


## 注意
- 单次发送数据 < MTU ，不支持截断传输
- 使用input与handler必须同线程


## 使用
1. 创建sarq对象、缓存区
2. 初始化对象
3. 轮询处理函数，收到数据时调用input函数
4. 操作读写API
```c
sarq_t sarq;
u8 sarq_buf[1024];
sarq_init(&sarq, sarq_buf, sizeof(sarq_buf), 1, 2, 10, output);

err_t err;
const u8 test_str[] = "0123456789ABCDEF";
err = sarq_write(&sarq, test_str, sizeof(test_str));
printf("write ok is 0=%d\n", err);

while (1)
{
    sarq_handler(&sarq, GetTickCount());

    u8 buf[1024];
    u16 len;
    len = sarq_read(&sarq, buf, sizeof(buf));
    if (len)
        printf("get:%d-%s\n", len, buf);
}
```

## 演示
<p align="center">
<img src="docs/image/模拟测试效果图.PNG">
</p>