/**
 *      自定义断言
 * 
 * 用于调试
 * By：fishc  2021.03
*/

#ifndef _DB_ASSERT_H
#define _DB_ASSERT_H

#ifdef __cplusplus
extern "C"
{
#endif

// #define NDEBUG

#ifdef NDEBUG
#define DB_ASSERT(condition) ((void)0)
#else
#include "stdio.h"
#define _db_assert(file, line) printf("assert: %s,%d\r\n", file, line)

#define DB_ASSERT(condition)            \
    if (condition)                      \
        (void)0;                        \
    else                                \
    {                                   \
        _db_assert(__FILE__, __LINE__); \
        while (1)                       \
            ;                           \
    }
#endif

#ifdef __cplusplus
}
#endif

#endif
