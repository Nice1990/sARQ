/**
 *      sARQ - 简单自动重发请求协议
 * 
 * By：fishc 2021.04
 * MIT license
*/

#ifndef _SARQ_H
#define _SARQ_H

// #ifdef __cplusplus
// extern "C"
// {
// #endif

#include "typedef.h"
#include "vfifo.h"

/* --------------------- config ------------------------------ */
#define SARQ_MTU 32   // byte
#define SARQ_USELOG 1 // 使用日志

/* --------------------- type ------------------------------ */
typedef void (*sarq_output_t)(const u8 *dat, u16 len); // 输出回调函数类型

#pragma pack(push)
#pragma pack(1)
typedef struct
{
    /* 内存与回调函数 */
    vfifo_t user_r;
    vfifo_t user_w;
    sarq_output_t output;

    /* 配置项 */
    struct
    {
        u16 repeat;
        u16 delay;
        u8 port; // 协议点对点端口
    } config;

    /* 包控制 */
    struct
    {
        u8 sn;            // 打包序号
        u8 last_sn;       // 重复包序号
        u16 len;          // 重发数据长度
        u16 count;        // 重发计数
        u8 wait_sn;       // 等待应答对应序号
        u32 time;         // 延时操作
        u8 buf[SARQ_MTU]; // 重发缓冲区
        u8 enable : 1;    // 使能标志，控制发送与接收
        u8 wait_ack : 1;  // 等待应答标志
    } ctrl;

#if SARQ_USELOG
    struct
    {
        u32 send;    // 发送不同包数量
        u32 perfect; // 单次发送完成数
        u32 repeat;  // 重发数
        u32 timeout; // 发送失败数
        u32 receive; // 接收不同包数
    } log;
#endif

} sarq_t;
#pragma pack(pop)

/* ---------------------- API ----------------------------- */
#if SARQ_USELOG
void sarq_info(sarq_t *obj);
#define SARQ_INFO(obj) sarq_info(obj)
#else
#define SARQ_INFO(obj) (void)0
#endif

/**
 * 初始化对象
 * @param ojb: 待初始化对象
 * @param buf_x2: 2倍的存储空间
 * @param buf_len: 总存储空间长度，字节
 * @param port: 协议端口；协议为点对点通信，必须端口相同
 * @param repeat: 重发次数
 * @param delay: 重发延时
 * @param output: 底层输出回调函数
*/
void sarq_init(sarq_t *obj, const u8 *buf_x2, u16 buf_len, u8 port,
               u16 repeat, u16 delay, const sarq_output_t output);

/**
 * 写入数据
 * @param dat: 数据地址
 * @param max: 写入字节数
 * @return: ERR_xx，如:ERR_OK
*/
err_t sarq_write(sarq_t *obj, const u8 *dat, vfifo_len_t len);

/**
 * 读取数据
 * @param dat: 数据存储地址
 * @param max: 存储区最大字节
 * @return: 读取字节数
*/
vfifo_len_t sarq_read(sarq_t *obj, u8 *dat, vfifo_len_t max);

/**
 * 协议缓存清空
 * @note: 只清空缓存并复位相关中间控制
*/
void sarq_clean(sarq_t *obj);

/**
 * 协议处理
 * 用于管理发送
 * @param current_ms: 当前系统时间，ms
 * @note: 需要轮询调用
*/
void sarq_handler(sarq_t *obj, u32 current_ms);

/**
 * 数据输入
 * @param obj: 协议对象地址
 * @param dat: 收到的数据起始地址
 * @param len: 数据长度，字节
 * @note：用户收到数据帧后调用此函数
 *  内部使用了output函数，input与handler必须同线程
*/
void sarq_input(sarq_t *obj, u8 *dat, u16 len);

#if __cplusplus
}
#endif

#endif
