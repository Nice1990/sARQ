/**
 * 			可变数据先入先出队列
 * 
 * By：fishc  2021.03
 * MIT licence
 */

#include "vfifo.h"
#include "db_assert.h"
#include "typedef.h"

/* ---------------------------------------------------------- */

#define ALWAY_0XE1 0xE1

/* byte align */
#pragma pack(push)
#pragma pack(1)
typedef struct
{
    vfifo_len_t len;
    u8 alway_0xe1; // 数据有效标志位，必须在最后
} data_head_t;     /* data head */
#pragma pack(pop)

/* ---------------------------------------------------------- */

/**
 * 获取队列中现存数据大小
 * @param fifo：队列指针
 * @return : 单位字节
*/
static inline vfifo_len_t vfifo_get_size(const vfifo_t *fifo)
{
    vfifo_len_t ret;
    DB_ASSERT(fifo);

    ret = fifo->len - fifo->front + fifo->rear;
    ret %= fifo->len;
    return ret;
}

static void ring_push(const u8 *src, vfifo_len_t len, u8 *dest, vfifo_len_t dest_index, vfifo_len_t max)
{
    vfifo_len_t i;

    DB_ASSERT(src && dest);
    DB_ASSERT(max > dest_index);

    for (i = 0; i < len; i++)
    {
        dest[dest_index] = src[i];
        dest_index++;
        dest_index %= max;
    }
}

static void ring_pop(const u8 *src, vfifo_len_t src_index, vfifo_len_t max, u8 *dest, vfifo_len_t len)
{
    vfifo_len_t i;

    DB_ASSERT(src && dest);
    DB_ASSERT(max > src_index);

    for (i = 0; i < len; i++)
    {
        dest[i] = src[src_index];
        src_index++;
        src_index %= max;
    }
}

/* ---------------------------------------------------------- */

/**
 * 入队
 * @param fifo：队列指针
 * @param dat：任意类型数据地址
 * @return : 状态，ERR_xx
*/
err_t vfifo_push(vfifo_t *fifo, const u8 *dat, vfifo_len_t len)
{
    data_head_t head;
    vfifo_len_t index;
    vfifo_len_t rear;

    DB_ASSERT(dat && fifo);
    DB_ASSERT(fifo->buf);

    if (len == 0)
        return ERR_OK;

    /* 临界区内管理，外部写入数据；保证可靠性与性能 */
    VFIFO_ENTER_CRITICAL();

    if (vfifo_get_size(fifo) + len + sizeof(data_head_t) + fifo->pop >= fifo->len)
    {
        VFIFO_LEAVE_CRITICAL();
        return ERR_OVERFLOW;
    }
    rear = fifo->rear;
    index = rear + sizeof(data_head_t);
    index %= fifo->len;

    /* 清除数据标志位 */
    head.alway_0xe1 = 0;
    ring_push((u8 *)&head, sizeof(data_head_t), fifo->buf, rear, fifo->len);

    /* 最后修改索引 */
    fifo->rear = (len + rear + sizeof(data_head_t)) % fifo->len;
    VFIFO_LEAVE_CRITICAL();

    /* 写入具体数据 */
    ring_push(dat, len, fifo->buf, index, fifo->len);

    /* 最后写入数据头，并标明数据有效 */
    head.alway_0xe1 = ALWAY_0XE1;
    head.len = len;
    ring_push((u8 *)&head, sizeof(data_head_t), fifo->buf, rear, fifo->len);

    return ERR_OK;
}

/**
 * 只查看最前面一个数据，不取出
 * @param fifo：队列指针
 * @param dat：数据存放地址，=NULL时不拷贝数据（此时len无效）
 * @param len：存储区长度
 * @return : 取出字节数
 * @note ：溢出时返回值=0
*/
vfifo_len_t vfifo_peek(vfifo_t *fifo, u8 *dat, vfifo_len_t len)
{
    vfifo_len_t index;
    data_head_t head;

    DB_ASSERT(fifo);
    DB_ASSERT(fifo->buf);

    /* 临界区内管理，外部写入数据；保证可靠性与性能 */
    VFIFO_ENTER_CRITICAL();

    if (vfifo_get_size(fifo) < sizeof(data_head_t))
    {
        VFIFO_LEAVE_CRITICAL();
        return 0;
    }

    ring_pop(fifo->buf, fifo->front, fifo->len, (u8 *)&head, sizeof(data_head_t));
    index = fifo->front + sizeof(data_head_t);
    index %= fifo->len;

    VFIFO_LEAVE_CRITICAL();

#if !VFIFO_USE_CRITICAL
    /* 二次读取，防止多线程时错误 */
    if (head.alway_0xe1 == ALWAY_0XE1)
        ring_pop(fifo->buf, fifo->front, fifo->len, (u8 *)&head, sizeof(data_head_t));
#endif

    if (head.alway_0xe1 != ALWAY_0XE1)
        return 0;
    else if (dat == NULL)
        return head.len;
    else if (len < head.len)
        return 0;

    ring_pop(fifo->buf, index, fifo->len, dat, head.len);

    return head.len;
}

/**
 * 出队
 * @param fifo：队列指针
 * @param dat：数据存放地址，=NULL时不拷贝数据（此时len无效）
 * @param len：存储区长度
 * @return : 取出字节数
 * @note ：溢出时返回值=0
*/
vfifo_len_t vfifo_pop(vfifo_t *fifo, u8 *dat, vfifo_len_t len)
{
    vfifo_len_t index;
    data_head_t head;

    DB_ASSERT(fifo);
    DB_ASSERT(fifo->buf);

    /* 临界区内管理，外部写入数据；保证可靠性与性能 */
    VFIFO_ENTER_CRITICAL();

    if (vfifo_get_size(fifo) < sizeof(data_head_t))
    {
        VFIFO_LEAVE_CRITICAL();
        return 0;
    }

    ring_pop(fifo->buf, fifo->front, fifo->len, (u8 *)&head, sizeof(data_head_t));

#if !VFIFO_USE_CRITICAL
    /* 二次读取，防止多线程时错误 */
    if (head.alway_0xe1 == ALWAY_0XE1)
        ring_pop(fifo->buf, fifo->front, fifo->len, (u8 *)&head, sizeof(data_head_t));
#endif

    if (head.alway_0xe1 != ALWAY_0XE1)
    {
        VFIFO_LEAVE_CRITICAL();
        return 0;
    }

    /* 临界区内记录待取数据 */
    fifo->pop += head.len;

    /* 管理队列索引 */
    index = fifo->front + sizeof(data_head_t);
    index %= fifo->len;
#if VFIFO_USE_CRITICAL
    fifo->front = (head.len + fifo->front + sizeof(data_head_t)) % fifo->len;
#endif

    VFIFO_LEAVE_CRITICAL();

    if (dat == NULL)
    {
        fifo->pop -= head.len;
        return 0;
    }

    /* 队列中数据长度超过缓冲区，错误情况 */
    if (len < head.len)
    {
        DB_ASSERT(0);
        return 0;
    }

    ring_pop(fifo->buf, index, fifo->len, dat, head.len);
    fifo->pop -= head.len;

#if !VFIFO_USE_CRITICAL
    fifo->front = (head.len + fifo->front + sizeof(data_head_t)) % fifo->len;
#endif

    return head.len;
}

/**
 * 在队列前面入队
 * @param fifo：队列指针
 * @param dat：任意类型数据地址
 * @return : 状态，ERR_xx
 * @note ：插队到最前面，特殊需求
*/
err_t vfifo_push_front(vfifo_t *fifo, const u8 *dat, vfifo_len_t len)
{
    data_head_t head;
    vfifo_len_t index, bk;

    DB_ASSERT(dat && fifo);
    DB_ASSERT(fifo->buf);

    if (len == 0)
        return ERR_OK;

    /* 临界区内管理，外部写入数据；保证可靠性与性能 */
    VFIFO_ENTER_CRITICAL();

    if (vfifo_get_size(fifo) + len + sizeof(data_head_t) >= fifo->len)
    {
        VFIFO_LEAVE_CRITICAL();
        return ERR_OVERFLOW;
    }

    /* 管理队列索引 */
    index = fifo->len - len + fifo->front - sizeof(data_head_t);
    index %= fifo->len;

    /* 清除数据标志位 */
    head.alway_0xe1 = 0;
    ring_push((u8 *)&head, sizeof(data_head_t), fifo->buf, index, fifo->len);

    fifo->front = index;
    VFIFO_LEAVE_CRITICAL();

    /* 写入数据 */
    bk = index;
    index += sizeof(data_head_t);
    index %= fifo->len;
    ring_push((u8 *)dat, len, fifo->buf, index, fifo->len);

    /* 最后写入数据头 */
    head.len = len;
    head.alway_0xe1 = ALWAY_0XE1;
    ring_push((u8 *)&head, sizeof(data_head_t), fifo->buf, bk, fifo->len);
    return ERR_OK;
}

/**
 * 清空队列
 * @param fifo：队列指针
 * @return : 状态，ERR_xx
*/
err_t vfifo_clean(vfifo_t *fifo)
{
    DB_ASSERT(fifo);
    DB_ASSERT(fifo->buf);

    VFIFO_ENTER_CRITICAL();

    fifo->rear = fifo->front = 0;

    VFIFO_LEAVE_CRITICAL();

    return ERR_OK;
}

/**
 * 初始化队列
 * @param fifo: 队列指针，需要外部提供实体
 * @param buf: 缓存区指针，需要外部提供缓冲区空间
 * @param len: 缓冲区大小，单位字节
 * @return : 状态，ERR_xx
 * 
*/
err_t vfifo_init(vfifo_t *fifo, const u8 *buf, vfifo_len_t len)
{
    DB_ASSERT(buf && fifo);

    if (len < sizeof(data_head_t) * 3)
    {
        DB_ASSERT(0); // 缓存太小
        return ERR_PARAM;
    }

    fifo->rear = fifo->front = 0;
    fifo->len = len - 1;
    fifo->buf = (u8 *)buf;
    fifo->pop = 0;

    return ERR_OK;
}

/**
 * 获取剩余空间
 * @param fifo: 队列指针
 * @return : 剩余可存数据字节数
*/
vfifo_len_t vfifo_get_remain(const vfifo_t *fifo)
{
    vfifo_len_t len;
    DB_ASSERT(fifo);

    VFIFO_ENTER_CRITICAL();

    len = vfifo_get_size(fifo);

    VFIFO_LEAVE_CRITICAL();

    len = fifo->len - len - sizeof(data_head_t);
    return len;
}
