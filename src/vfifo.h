/**
 * 			可变数据先入先出队列
 * 1. 顺序环形队列
 * 2. 支持任意可变长度类型
 * 3. 支持RTOS、裸机、中断
 * 4. 静态内存管理，无内存碎片
 * 
 *  注意：
 * 1. 使用vfifo_get_remain()，判断剩余数据存储空间
 * 
 * By：fishc  2021.03
 * MIT licence
 */

#ifndef _VFIFO_H
#define _VFIFO_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "typedef.h"

/* -------------------------- Config -------------------------------- */
/* 队列长度所需的变量类型 */
typedef u16 vfifo_len_t;

/* 临界区功能，保证多操作安全（可多个出入队操作）；不提供则只支持单处调用出入队（如中断中入队，主循环出队） */
#define VFIFO_USE_CRITICAL 0
#if VFIFO_USE_CRITICAL
#define VFIFO_ENTER_CRITICAL() __disable_irq()
#define VFIFO_LEAVE_CRITICAL() __enable_irq()
#else
#define VFIFO_ENTER_CRITICAL() (void)0
#define VFIFO_LEAVE_CRITICAL() (void)0
#endif

/* ---------------------------------------------------------- */
typedef struct
{
    vfifo_len_t rear;
    vfifo_len_t front;
    vfifo_len_t pop; /* pop remain, byte*/
    vfifo_len_t len; /* byte */
    u8 *buf;         /* buffer address */
} vfifo_t;

/* ------------------------ API ---------------------------------- */

/* 直接静态创建，用法：vfifo_t vff = VFIFO_ADAPTOR(tab, sizeof(tab)); */
#define VFIFO_ADAPTOR(buf_addr, buf_len) {.rear = 0, .front = 0, .buf = buf_addr, .len = (buf_len - 1), .pop = 0}

/**
 * 初始化队列
 * @param fifo: 队列指针，需要外部提供实体
 * @param buf: 缓存区指针，需要外部提供缓冲区空间
 * @param len: 缓冲区大小，单位字节
 * @return : 状态，ERR_xx
 * 
*/
err_t vfifo_init(vfifo_t *fifo, const u8 *buf, const vfifo_len_t len);

/**
 * 入队
 * @param fifo：队列指针
 * @param dat：任意类型数据地址
 * @return : 状态，ERR_xx
*/
err_t vfifo_push(vfifo_t *fifo, const u8 *dat, vfifo_len_t len);

/**
 * 出队
 * @param fifo：队列指针
 * @param dat：数据存放地址，=NULL时不拷贝数据（此时len无效）
 * @param len：存储区长度
 * @return : 取出字节数
 * @note ：溢出时返回值=0
*/
vfifo_len_t vfifo_pop(vfifo_t *fifo, u8 *dat, vfifo_len_t len);

/**
 * 只查看最前面一个数据，不取出
 * @param fifo：队列指针
 * @param dat：数据存放地址，=NULL时不拷贝数据（此时len无效）
 * @param len：存储区长度
 * @return : 取出字节数
 * @note ：溢出时返回值=0
*/
vfifo_len_t vfifo_peek(vfifo_t *fifo, u8 *dat, vfifo_len_t len);

/**
 * 在队列前面入队
 * @param fifo：队列指针
 * @param dat：任意类型数据地址
 * @return : 状态，ERR_xx
 * @note ：插队到最前面，特殊需求
*/
err_t vfifo_push_front(vfifo_t *fifo, const u8 *dat, vfifo_len_t len);

/**
 * 清空队列
 * @param fifo：队列指针
 * @return : 状态，ERR_xx
*/
err_t vfifo_clean(vfifo_t *fifo);

/**
 * 获取剩余空间
 * @param fifo: 队列指针
 * @return : 剩余可存数据字节数
*/
vfifo_len_t vfifo_get_remain(const vfifo_t *fifo);

#if __cplusplus
}
#endif

#endif
