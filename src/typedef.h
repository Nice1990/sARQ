/**
 *      typedef - 类型定义
 * 
 * By：fishc  2021.03
*/

#ifndef _TYPEDEF_H
#define _TYPEDEF_H

#ifndef NULL
#define NULL (void *)0
#endif

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

/* base type */
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

/* opt result */
typedef u8 err_t;
enum
{
    ERR_OK = 0,   /* 无错误 */
    ERR_NULL,     /* 空指针 */
    ERR_PARAM,    /* 参数错误 */
    ERR_FULL,     /* 已满 */
    ERR_EMPTY,    /* 空 */
    ERR_OVERFLOW, /* 溢出 */
    ERR_TIMEOUT,  /* 超时 */
};

#endif
