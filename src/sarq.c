/**
 * sarq
*/

#include "sarq.h"
#include "vfifo.h"
#include "typedef.h"
#include "db_assert.h"
#include "stdlib.h"

/*--------------------- type -------------------------*/
#pragma pack(push)
#pragma pack(4)
typedef struct
{
    u8 checksum; // 此后所有数据之和
#if SARQ_DEFAULT_MTU < 0xff
    u8 len; // 包总长度
#else
    u16 len;
#endif

    u8 port;
    u8 cmd;
    u8 sn;
    u8 dat;
} pack_t;

typedef struct
{
#if SARQ_DEFAULT_MTU < 0xff
    u8 len; // dat数据长度
#else
    u16 len;
#endif

    u8 dat;
} head_t;

/* 数据结构：pack head+data head+data ... */
#pragma pack(pop)

enum PACK_CMD
{
    SARQ_CMD_NONE = 0,
    SARQ_CMD_DATA,
    SARQ_CMD_ACK,
};

#define OFFSET_SIZE(type, member) ((size_t)(&((type *)0)->member))

/*--------------------- static function -------------------------*/
#if SARQ_USELOG
#define _printf_log(...) printf(__VA_ARGS__)
#else
#define _printf_log() (void)0
#endif

static inline void set_rand_delay(sarq_t *obj, u32 current_ms)
{
    u32 d;

    DB_ASSERT(obj);

    srand(current_ms);
    d = obj->config.delay + rand() % (obj->config.delay + 1);
    obj->ctrl.time = current_ms + d;
}

static inline u8 delay_over(sarq_t *obj, u32 current_ms)
{
    return (obj->ctrl.time < current_ms ? TRUE : FALSE);
}

static u8 checksum_xor8(const u8 *dat, u16 len)
{
    u8 sum = 0;
    DB_ASSERT(dat);

    for (; len > 0; len--)
    {
        sum ^= *dat;
        dat++;
    }

    return sum;
}

// 打包到对应缓冲区，返回包总长度
static u16 package(sarq_t *obj)
{
    u16 len, sum;
    u8 *buf;
    pack_t *pack;
    head_t *head;
    u16 buf_remain;

    DB_ASSERT(SARQ_MTU >= OFFSET_SIZE(pack_t, dat) - OFFSET_SIZE(head_t, dat));
    DB_ASSERT(obj);

    pack = (pack_t *)obj->ctrl.buf;
    head = (head_t *)(&pack->dat);
    buf = (&head->dat);
    buf_remain = SARQ_MTU - OFFSET_SIZE(pack_t, dat) - OFFSET_SIZE(head_t, dat);

    /* mtu范围内，打包待发送数据 */
    sum = 0;
    while (1)
    {
        len = vfifo_peek(&obj->user_w, NULL, 0);
        if (len == 0 || len + OFFSET_SIZE(head_t, dat) > buf_remain)
            break;

        len = vfifo_pop(&obj->user_w, buf, buf_remain);
        sum += len + OFFSET_SIZE(head_t, dat);
        head->len = len;
        head = (head_t *)(buf + len);
        buf = &head->dat;
        buf_remain = buf_remain - len - OFFSET_SIZE(head_t, dat);
    }
    if (sum == 0)
        return 0;

    pack->port = obj->config.port;
    pack->cmd = SARQ_CMD_DATA;
    obj->ctrl.sn++;
    pack->sn = obj->ctrl.sn;
    obj->ctrl.wait_sn = pack->sn; // 打包后会立即发送，记录此sn

    sum += OFFSET_SIZE(pack_t, dat);
    pack->len = sum;
    pack->checksum = checksum_xor8((obj->ctrl.buf + sizeof(pack->checksum)), sum - sizeof(pack->checksum));

    return sum;
}

// 解包，返回获取到的数据字节
static u16 unpack(const u8 *dat, u16 len, vfifo_t *vff, pack_t *pkt_info)
{
    u16 sum = 0;
    head_t *head;
    pack_t *pack;
    u8 *buf;

    DB_ASSERT(dat && vff && pkt_info);

    pkt_info->cmd = SARQ_CMD_NONE;

    /* 数据错误 */
    if (len < OFFSET_SIZE(head_t, dat) + OFFSET_SIZE(pack_t, dat))
        return 0;

    pack = (pack_t *)dat;
    buf = (u8 *)dat + sizeof(pack->checksum);

    if (pack->len != len || checksum_xor8(buf, len - sizeof(pack->checksum)) != pack->checksum)
        return 0;

    head = (head_t *)(&pack->dat);
    buf = &head->dat;
    len = len - OFFSET_SIZE(pack_t, dat); // len=data length
    while (1)
    {
        u16 t;
        /* 数据判断，防止溢出 */
        t = head->len + OFFSET_SIZE(head_t, dat);
        if (len < t)
            break;
        len -= t;

        /* 取出数据存到队列中 */
        sum += head->len;
        vfifo_push(vff, buf, head->len);
        head = (head_t *)(buf + head->len);
        buf = &head->dat;
    }

    *pkt_info = *pack;
    return sum;
}

// 直接调用output发送应答，不支持中断
static void send_ack(const sarq_t *obj, u8 sn)
{
    pack_t *pack;
    head_t *head;
    u8 tab[OFFSET_SIZE(pack_t, dat) + OFFSET_SIZE(head_t, dat) + 1];
    u8 *buf;

    DB_ASSERT(obj);

    pack = (pack_t *)tab;
    head = (head_t *)(&pack->dat);
    head->len = 1;
    pack->cmd = SARQ_CMD_ACK;
    pack->sn = 0xff;
    pack->len = sizeof(tab);
    pack->port = obj->config.port;
    buf = &head->dat;
    *buf = sn;
    pack->checksum = checksum_xor8(tab + sizeof(pack->checksum), sizeof(tab) - sizeof(pack->checksum));

    obj->output(tab, sizeof(tab));
}

/*-------------------------------- public function --------------------------------------*/
#if SARQ_USELOG
void sarq_info(sarq_t *obj)
{
    _printf_log("\r\n=== info ===\r\n");
    _printf_log("send[%u], perfect[%u], repeat[%u], timeout[%u], receive[%u]\r\n",
                obj->log.send, obj->log.perfect, obj->log.repeat, obj->log.timeout, obj->log.receive);
}
#endif

/**
 * 初始化对象
 * @param ojb: 待初始化对象
 * @param buf_x2: 2倍的存储空间
 * @param buf_len: 总存储空间长度，字节
 * @param port: 协议端口；协议为点对点通信，必须端口相同
 * @param repeat: 重发次数
 * @param delay: 重发延时
 * @param output: 底层输出回调函数
*/
void sarq_init(sarq_t *obj, const u8 *buf_x2, u16 buf_len, u8 port,
               u16 repeat, u16 delay, const sarq_output_t output)
{
    DB_ASSERT(obj && buf_x2);
    DB_ASSERT(buf_len >= 100);

    /* 创建3个相同大小缓冲队列 */
    buf_len /= 2;
    vfifo_init(&obj->user_r, buf_x2, buf_len);
    buf_x2 += buf_len;
    vfifo_init(&obj->user_w, buf_x2, buf_len);

    /* 初始化配置 */
    obj->ctrl.count = 0;
    obj->ctrl.len = 0;
    obj->ctrl.sn = 0;
    obj->ctrl.time = 0;
    obj->ctrl.wait_sn = 0;
    obj->ctrl.wait_ack = FALSE;
    obj->ctrl.last_sn = 0;

    obj->output = output;
    obj->config.delay = delay;
    obj->config.repeat = repeat;
    obj->config.port = port;

#if SARQ_USELOG
    obj->log.repeat = 0;
    obj->log.perfect = 0;
    obj->log.timeout = 0;
    obj->log.send = 0;
    obj->log.receive = 0;
#endif

    obj->ctrl.enable = TRUE;
}

/**
 * 写入数据
 * @param dat: 数据地址
 * @param max: 写入字节数
 * @return: ERR_xx，如:ERR_OK
*/
err_t sarq_write(sarq_t *obj, const u8 *dat, vfifo_len_t len)
{
    err_t err;

    DB_ASSERT(obj && dat);
    DB_ASSERT(len < SARQ_MTU);

    err = vfifo_push(&obj->user_w, dat, len);
    return err;
}

/**
 * 读取数据
 * @param dat: 数据存储地址
 * @param max: 存储区最大字节
 * @return: 读取字节数
*/
vfifo_len_t sarq_read(sarq_t *obj, u8 *dat, vfifo_len_t max)
{
    vfifo_len_t len;

    DB_ASSERT(obj && dat);

    len = vfifo_pop(&obj->user_r, dat, max);
    return len;
}

/**
 * 协议缓存清空
 * @note: 只清空缓存并复位相关中间控制
*/
void sarq_clean(sarq_t *obj)
{
    DB_ASSERT(obj);

    vfifo_clean(&obj->user_r);
    vfifo_clean(&obj->user_w);

    obj->ctrl.wait_ack = FALSE;
    obj->ctrl.count = 0;
    obj->ctrl.wait_sn = 0xed;
    obj->ctrl.time = 0;
}

/**
 * 协议处理
 * 用于管理发送
 * @param current_ms: 当前系统时间，ms
 * @note: 需要轮询调用
*/
void sarq_handler(sarq_t *obj, u32 current_ms)
{
    u16 len;

    DB_ASSERT(obj);

    if (delay_over(obj, current_ms) == FALSE || obj->ctrl.enable == FALSE)
        return;

    /* 有需求时发送数据 */
    if (obj->ctrl.wait_ack == FALSE)
    {
        len = package(obj);
        if (len)
        {
            obj->ctrl.len = len;
            obj->ctrl.wait_ack = TRUE;
            obj->ctrl.count = 0;

            obj->output(obj->ctrl.buf, obj->ctrl.len);
            set_rand_delay(obj, current_ms);

#if SARQ_USELOG
            obj->log.send++;
#endif
        }
    }

    /* 等待应答，随机延时重发，超过次数结束重发 */
    else
    {
        if (obj->ctrl.count >= obj->config.repeat)
        {
#if SARQ_USELOG
            _printf_log("\r\n=== timeout ===\r\n");
            _printf_log("sn: %u, data: ", obj->ctrl.wait_sn);
            for (int i = 0; i < obj->ctrl.len; i++)
            {
                _printf_log("%02x ", obj->ctrl.buf[i]);
            }
            _printf_log("\r\n");

            obj->log.timeout++;
#endif

            obj->ctrl.wait_ack = FALSE;
            obj->ctrl.len = 0;
        }
        else
        {
            obj->ctrl.count++;
            obj->output(obj->ctrl.buf, obj->ctrl.len);
            set_rand_delay(obj, current_ms);

#if SARQ_USELOG
            obj->log.repeat++;
#endif
        }
    }
}

/**
 * 数据输入
 * @param obj: 协议对象地址
 * @param dat: 收到的数据起始地址
 * @param len: 数据长度，字节
 * @note：用户收到数据帧后调用此函数
 *  内部使用了output函数，input与handler必须同线程
*/
void sarq_input(sarq_t *obj, u8 *dat, u16 len)
{
    pack_t pkt_info;
    u8 vff_buf[SARQ_MTU * 2];
    u8 buf[SARQ_MTU];

    DB_ASSERT(obj && dat);

    if (obj->ctrl.enable == FALSE)
        return;

    /* 解包 */
    vfifo_t vff = VFIFO_ADAPTOR(vff_buf, sizeof(vff_buf));
    len = unpack(dat, len, &vff, &pkt_info);
    if (len == 0)
        return;

    if (pkt_info.port != obj->config.port)
        return;
    else if (pkt_info.cmd == SARQ_CMD_ACK)
    {
        len = vfifo_pop(&vff, buf, sizeof(buf));
        if (len == 1 && buf[0] == obj->ctrl.wait_sn)
        {
            obj->ctrl.wait_ack = FALSE;
            obj->ctrl.time = 0;
            obj->ctrl.wait_sn = 0xed;

#if SARQ_USELOG
            if (obj->ctrl.count == 0)
                obj->log.perfect++;
#endif
        }
    }
    else if (pkt_info.cmd == SARQ_CMD_DATA)
    {
        send_ack(obj, pkt_info.sn); // 收到数据后立刻发送应答，实现停等协议

        /* 重复包判断 */
        if (obj->ctrl.last_sn == pkt_info.sn)
            return;
        obj->ctrl.last_sn = pkt_info.sn;
#if SARQ_USELOG
        obj->log.receive++;
#endif

        /* 存入数据 */
        while (1)
        {
            len = vfifo_pop(&vff, buf, sizeof(buf));
            if (len == 0)
                break;

            vfifo_push(&obj->user_r, buf, len);
        }
    }
}
