/**
 *   test
*/

#include "../src/sarq.h"
#include "../src/vfifo.h"
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include "share_mem.h"
#include "string.h"

/* 2点相互测试 */
const char *sm_name[2] = {"test/sarq", "test/sarq2"}; // 共享内存的名字
sm_t sm[2];
char *sm_buf[2];
int host_id = 0;
vfifo_t *vff[2];
HANDLE g_mutex;

typedef struct
{
    u16 len;
    u8 dat;
} smbuf_t;

void output(const u8 *dat, u16 len)
{
    /* 交叉发送 */
    // WaitForSingleObject(g_mutex, INFINITE); //等待互斥量  INFINITE表示永远等待
    vfifo_push(vff[(host_id + 1) % 2], dat, len);
    // ReleaseMutex(g_mutex);

#if 0
    for (int i = 0; i < len; i++)
    {
        if (i % 16 == 0)
            putch('\n');
        printf("%02x ", dat[i]);
    }
    putch('\n');
#endif
}

// 获得高精度ms
double get_win_ms()
{
    __int64 Freq = 0;
    __int64 Count = 0;

    if (QueryPerformanceFrequency((LARGE_INTEGER *)&Freq) &&
        Freq > 0 && QueryPerformanceCounter((LARGE_INTEGER *)&Count))
    {
        // printf("freq:%ld  ", Freq);
        //乘以1000，把秒化为毫秒
        return (double)Count / (double)Freq * 1000.0;
    }
    else
        return (double)GetTickCount();

    return 0.0;
}

/* --------------------------------------------------------------- */
int main(int argc, char *args[])
{
    for (int i = 1; i < argc; i++)
        printf("%d: %s\t", i, args[i]);

    if (argc >= 2)
        host_id = atoi(args[1]);
    host_id %= 2;
    printf("host=[%d]\n", host_id);

    // 创建共享内存
    for (int i = 0; i < 2; i++)
    {
        sm_buf[i] = sm_creat(sm_name[i], 1024, &sm[i]);
        printf("sm_buf[%d]:%x\t", i, (int)sm_buf[i]);

        vff[i] = (vfifo_t *)sm_buf[i];
        vfifo_init(vff[i], (u8 *)sm_buf[i] + sizeof(vfifo_t), 1024 - sizeof(vfifo_t));
    }

    // 创建进程间互斥锁
    g_mutex = CreateMutex(NULL, FALSE, "sarq/mutex");

    sarq_t sarq;
    u8 sarq_buf[1024];
    sarq_init(&sarq, sarq_buf, sizeof(sarq_buf), 1, 2, 10, output);

    err_t err = ERR_OK;
    const u8 test_str[] = "0123456789ABCDEF";
    err += sarq_write(&sarq, test_str, sizeof(test_str));
    err += sarq_write(&sarq, test_str, sizeof(test_str));
    err += sarq_write(&sarq, (u8 *)"123", 4);
    printf("write ok is 0=%d\n", err);

    srand(GetTickCount());

    char batch_flag = 0;
    while (1)
    {
        // 按键输入
        if (_kbhit())
        {
            static int count = 0;
            static u8 buf[1024];
            int c;
            c = getch();
            putch(c);
            if (c == 'q')
                break;
            else if (c == '\r')
            {
                putch('\n');
                sarq_write(&sarq, buf, count);
                count = 0;
            }
            else if (c == '@')
            {
                batch_flag = !batch_flag;
                if (batch_flag)
                    printf("batch wirte test: ---------\n");
                else
                    printf("end -----------------------\n");
            }
            else if (c == '!')
            {
                SARQ_INFO(&sarq);
            }
            else
            {
                buf[count] = c;
                count++;
            }
        }

        /* 批量发送 */
        static u32 time_batch = 0;
        if (batch_flag && time_batch < get_win_ms())
        {
            time_batch = get_win_ms() + 0;
            for (int i = 0; i < rand() % 3; i++)
            {
                char buf[10];
                int len = sprintf(buf, "@%05d", i);
                err_t err = sarq_write(&sarq, (u8 *)buf, len);
                // if (err != ERR_OK)
                //     batch_flag = !batch_flag;
            }
        }

        /* 模拟数据输入，跨进程队列，使用互斥锁 */
        u8 buf[1024];
        u16 len;
        // WaitForSingleObject(g_mutex, INFINITE); // 等待互斥量  INFINITE表示永远等待
        len = vfifo_pop(vff[host_id], buf, sizeof(buf));
        // ReleaseMutex(g_mutex);
        if (len)
        {
            sarq_input(&sarq, buf, len);
        }

        /* 接收数据测试 */
        len = sarq_read(&sarq, buf, sizeof(buf));
        if (len)
        {
            if (buf[0] != '@')
                buf[len] = 0, printf("get:%d-%s\n", len, buf);
            else
            {
                // static int count = 0;
                // count++;
                // if (count % 100 == 0)
                //     printf("@[%d]\n", count);
            }
        }

        /* 轮询 */
        sarq_handler(&sarq, (u32)get_win_ms());

/* 定时打印信息 */
#if 0
        static double info_time = 0;
        if (get_win_ms() > info_time)
        {
            info_time = get_win_ms() + 3000;
            SARQ_INFO(&sarq);
        }
#endif
    }

    // 释放共享内存
    for (int i = 0; i < 2; i++)
    {
        sm_delete(&sm[i]);
    }

    return 0;
}
