#ifndef __SHARE_MEM_H
#define __SHARE_MEM_H

#include <windows.h>

typedef struct
{
    HANDLE hMapFile;
    char *buf;
} sm_t;

/**
 * 创建共享内存
 * @param name：名字字符串，如："test/rf433"
 * @param size：内存大小，字节
 * @param sm：管理句柄
 * @return: 获得的内存地址
*/
char *sm_creat(const char *name, int size, sm_t *sm);

void sm_delete(const sm_t *sm);

#endif
