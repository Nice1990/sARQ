#include "share_mem.h"
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include "assert.h"

/**
 * 创建共享内存
 * @param name：名字字符串，如："test/rf433"
 * @param size：内存大小，字节
 * @param sm：管理句柄
 * @return: 获得的内存地址
*/
char *sm_creat(const char *name, int size, sm_t *sm)
{
    assert(name && sm);
    char *buf;

    // 创建共享文件句柄
    sm->hMapFile = CreateFileMapping(
        INVALID_HANDLE_VALUE, // 物理文件句柄
        NULL,                 // 默认安全级别
        PAGE_READWRITE,       // 可读可写
        0,                    // 高位文件大小
        size,                 // 地位文件大小
        name                  // 共享内存名称
    );

    buf = (char *)MapViewOfFile(
        sm->hMapFile,        // 共享内存的句柄
        FILE_MAP_ALL_ACCESS, // 可读写许可
        0,
        0,
        size);

    sm->buf = buf;

    return buf;
}

void sm_delete(const sm_t *sm)
{
    assert(sm);

    if (sm->buf)
        UnmapViewOfFile(sm->buf);
    if (sm->hMapFile)
        CloseHandle(sm->hMapFile);
}